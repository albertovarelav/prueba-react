module.exports = {
    testTimeout: 15000,
    roots: ['<rootDir>/src'],
    testEnvironment: '<rootDir>/src/utils/custom-test-env.ts',
    setupFiles: ['dotenv/config'],
    setupFilesAfterEnv: ['<rootDir>/jest-setup.ts'],
    transform: {
        '^.+\\.tsx?$': [
            'ts-jest',
            {
                tsconfig: 'tsconfig.jest.json',
            },
        ],
        '.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub',
    },

    testMatch: ['**/?(*.)(spec|test).ts?(x)'],

    modulePathIgnorePatterns: ['node_modules'],

    coveragePathIgnorePatterns: [
        '/node_modules/',
        '<rootDir>/src/.*/*.mdx',
        '<rootDir>/src/.*/*.styles.ts',
        '<rootDir>/src/assets/',
        '<rootDir>/src/theme/',
    ],

    moduleDirectories: ['<rootDir>/src', 'node_modules'],

    moduleNameMapper: {
        '\\.(css|scss)$': 'identity-obj-proxy',
        '\\.svg': '<rootDir>/src/utils/testSvgMock.ts',
        '@/(.*)': '<rootDir>/src/$1',
    },

    reporters: ['default'],
};
