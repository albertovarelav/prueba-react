const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
//const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');

module.exports = () => ({
    module: {
        rules: [
            {
                test: /\.s?css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader'],
                exclude: /\.module\.css$/,
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                use: [
                    {
                        loader: 'file-loader',
                    },
                ],
            },
            {
                test: /\.(woff2|woff|eot|ttf|otf|png)$/,
                use: ['file-loader'],
            },
            {
                test: /\.svg$/,
                use: [
                    {
                        loader: '@svgr/webpack',
                        options: {
                            icon: true,
                        },
                    },
                ],
            },
        ],
    },
    plugins: [
        new MiniCssExtractPlugin(),
        new CleanWebpackPlugin(),
        new CopyPlugin({
            patterns: [{ from: 'src/server.js', to: './' }],
        }),
        new webpack.IgnorePlugin({
            resourceRegExp: /\.test(.ts|.tsx)?$/,
        }),
    ],
    performance: {
        hints: false,
    },
    // externals: {
    //     react: 'react',
    //     'react-dom': 'reactDom',
    // },
    optimization: {
        //minimizer: [new TerserJSPlugin({ extractComments: false }), new OptimizeCSSAssetsPlugin()],
        minimizer: [new TerserJSPlugin({ extractComments: false }), new CssMinimizerPlugin()],
        runtimeChunk: 'single',
        splitChunks: {
            chunks: 'all',
            maxInitialRequests: Infinity,
            cacheGroups: {
                assets: {
                    test: /[\\/]src[\\/]assets[\\/]/,
                    name: 'assets',
                    chunks: 'all',
                    minSize: 0,
                },
                components: {
                    test: /[\\/]src[\\/]components[\\/]/,
                    name: 'components',
                    chunks: 'all',
                    minSize: 0,
                },
                styles: {
                    test: /.+\.styles\.ts/,
                    name: 'styles',
                    chunks: 'all',
                    minSize: 0,
                },
                mui: {
                    test: /[\\/]node_modules[\\/]@material-ui[\\/]/,
                    name(module) {
                        // get the name. E.g. node_modules/packageName/not/this/part.js
                        // or node_modules/packageName
                        const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];

                        // npm package names are URL-safe, but some servers don't like @ symbols
                        return `core.${packageName.replace('@', '')}`;
                    },
                },
                // commons: { test: /[\\/]node_modules[\\/]/, name: 'vendors', chunks: 'all' },
            },
        },
    },
});
