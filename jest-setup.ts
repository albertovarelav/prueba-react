/* eslint-disable no-console */
import 'regenerator-runtime/runtime';
import 'core-js/stable';
import '@testing-library/jest-dom/extend-expect';

const originalError = console.error;
beforeEach(() => {
    jest.spyOn(console, 'warn').mockImplementation(() => {});
});

beforeAll(() => {
    jest.setTimeout(30000);
    console.error = (...args): void => {
        if (/Warning.*not wrapped in act/.test(args[0])) {
            return;
        }
        if (/Warning.*perform a React state/.test(args[0])) {
            return;
        }
        originalError.call(console, ...args);
    };
});

afterAll(() => {
    console.error = originalError;
});
