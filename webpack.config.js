const path = require('path');
const webpack = require('webpack');
const { merge } = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const modeConfig = (env) => require(`./build-utils/webpack.${env}`)();
const presetConfig = require('./build-utils/loadPresets');
const Dotenv = require('dotenv-webpack');
const publicPath = (filename) => path.join(__dirname, `public/${filename}`);
const CopyPlugin = require('copy-webpack-plugin');

module.exports = ({ mode, presets } = { mode: 'production', presets: [] }) =>
    merge(
        {
            mode,
            entry: './src/App.tsx',
            output: {
                path: path.join(__dirname, 'dist'),
                filename: 'bundle.[name].[fullhash:8].js',
                chunkFilename: '[name].[fullhash:8].chunk.js',
                publicPath: '/',
            },
            module: {
                rules: [
                    {
                        test: /\.(ts|tsx)$/,
                        exclude: /node_modules/,
                        loader: 'babel-loader',
                    },
                ],
            },
            resolve: {
                extensions: ['.js', '.json', '.ts', '.tsx'],
                modules: ['node_modules'],
                unsafeCache: true,
                alias: {
                    '@': path.join(__dirname, 'src'),
                    'react/jsx-runtime.js': 'react/jsx-runtime',
                    'react/jsx-dev-runtime.js': 'react/jsx-dev-runtime',
                },
                fallback: {
                    fs: false,
                    crypto: false,
                },
            },
            plugins: [
                new HtmlWebpackPlugin({
                    template: publicPath('index.html'),
                    favicon: publicPath('favicon.ico'),
                }),
                new webpack.ProgressPlugin(),
                new Dotenv(),
                new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /es|en/),
            ],
        },
        modeConfig(mode),
        presetConfig({ mode, presets }),
    );
