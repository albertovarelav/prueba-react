# Mango technical test

This is de react app project for the Mango technical test.

## Installation

`yarn install`

## Configuration

The basic configurations for the application are made using the .env file
The .env file must necessarily have the following variables configured:

```
API_URL = [Api URL]
BASE_PATH = [Base path of the application]
```

## Available Scripts

In the project directory, you can run:

### `yarn serve`

Runs the app in the development mode.\
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

### `yarn test`

Launches the test runner in the interactive watch mode.\

### `yarn build`

Builds the app for production to the `dist` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

### `yarn build:dev`

Builds the app for development to the `dist` folder.\

### `yarn start`

Run builded app

### `yarn lint`

Find and fix problems on code

### `yarn postinstall`

Install husky hook
