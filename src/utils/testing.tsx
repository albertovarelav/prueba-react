/* eslint-disable no-console */
import React, { ReactElement } from 'react';

import { I18nextProvider } from 'react-i18next';
import { BrowserRouter as Router } from 'react-router-dom';

import i18n from '@/i18n';

interface TestingWrappersProps {
    children?: JSX.Element;
}

/**
 * Wrapper used in the tests to encapsulate the components in the providers used in the application
 * @param \{children\} ReactElement to be rendered inside the wrapper
 * @returns ReactElement\<TestingWrappersProps\>
 */
export function TestingWrappers({ children }: TestingWrappersProps): ReactElement<TestingWrappersProps> {
    return (
        <I18nextProvider i18n={i18n}>
            <Router>{children}</Router>
        </I18nextProvider>
    );
}
