import JSDOMEnvironment from 'jest-environment-jsdom';
import { TextEncoder } from 'util';

/**
 * A custom environment to set the TextEncoder
 */
class CustomTestEnvironment extends JSDOMEnvironment {
    constructor({ globalConfig, projectConfig }, context) {
        super({ globalConfig, projectConfig }, context);
        if (typeof this.global.TextEncoder === 'undefined') {
            this.global.TextEncoder = TextEncoder;
        }
    }
}
module.exports = CustomTestEnvironment;
