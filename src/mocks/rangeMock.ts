/**
 * Mock used in the tests with the necessary information used to create a Normal Range
 */
export const normalRangeMock = {
    min: 1,
    max: 100,
};

/**
 * Mock used in the tests with the necessary information used to create a Fixed Values Range
 */
export const fixedValuesRangeMock = {
    min: 1.99,
    max: 70.99,
    rangeValues: [1.99, 5.99, 10.99, 30.99, 50.99, 70.99],
};

/**
 * Mock used in the tests with the necessary information to show error message
 */
export const errorMock = {
    min: 50,
    max: 2,
};
