/**
 * Range structure obtained from API
 */
export interface Range {
    min: number;
    max: number;
    rangeValues?: number[];
}
