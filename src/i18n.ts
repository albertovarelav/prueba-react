import i18n from 'i18next';

import { es, en } from '@/assets/locales';
import { initReactI18next } from 'react-i18next';

i18n.use(initReactI18next).init({
    // i18n.init({
    resources: {
        es: es,
        en: en,
    },
    lng: 'en',
    ns: ['translations'],
    defaultNS: 'translations',
    debug: false,
    fallbackLng: 'en',
    nsSeparator: false,
    // keySeparator: false, // para que funcionen las keys anidadas no puede estar a false este valor
    keySeparator: '.',
    interpolation: {
        escapeValue: false, // not needed for react as it escapes by default
        formatSeparator: ',',
    },

    react: {
        useSuspense: false,
    },
});

export default i18n;
