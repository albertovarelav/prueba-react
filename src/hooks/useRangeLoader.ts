import { useCallback, useEffect, useState } from 'react';

import { Range } from '@/models';
import { getRange } from '@/api/range';

interface UseRangeLoaderResponse {
    range: Range;
    loading: boolean;
    loadRange: () => void;
}

/**
 * Hook in charge of obtaining a range via api according to the rangeId passed to it.
 * @param rangeId Id of the range to be loaded
 * @returns UseRangeLoaderResponse
 */
export const useRangeLoader = (rangeId: string): UseRangeLoaderResponse => {
    const [range, setRange] = useState<Range>(null);
    const [loading, setLoading] = useState<boolean>(false);

    const loadRange = useCallback(() => {
        (async (): Promise<void> => {
            setLoading(true);
            await getRange(rangeId)
                .then((responseRange) => {
                    setRange(responseRange);
                })
                .finally(() => {
                    setLoading(false);
                });
        })();
    }, [rangeId]);

    useEffect(() => {
        loadRange();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return { range, loading, loadRange };
};
