import React, { ReactElement } from 'react';

import { useTranslation } from 'react-i18next';

import { Range, Spinner } from '@/components';
import { useRangeLoader } from '@/hooks';

/**
 * Loads the information from the api corresponding to a Fixed Values Range and sends it to that component.
 * @returns ReactElement
 */
export const FixedValuesRange = (): ReactElement => {
    const [t] = useTranslation();
    const { loading, range } = useRangeLoader('exercise2');

    return (
        <>
            <h1>{t('page.exercise2.title')}</h1>
            {!!loading && <Spinner absolute />}
            {range && <Range>{range}</Range>}
        </>
    );
};
