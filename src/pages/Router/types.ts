/**
 * Interface that indicates the structure that the application routes will have
 */
export interface AppRoute {
    route: string;
    title?: string;
}

/**
 * Possible key for the application routes we will have
 */
export type AppRouteKey = 'home' | 'exercise1' | 'exercise2' | 'error';

/**
 * All the possible routes we will have and their structure
 */
export type AppRoutes = Record<AppRouteKey, AppRoute>;

/**
 * Defines the structure of routes
 */
export interface RouteType {
    path: string;
    component: string;
    componentPath: string;
    props?: any;
    exact?: boolean;
}
