import React from 'react';

import { render, screen, waitFor } from '@testing-library/react';

import i18n from '@/i18n';
import { AppRouter } from './AppRouter';

describe('AppRouter tests', () => {
    test('should render a AppRouter', async () => {
        render(<AppRouter />);

        await waitFor(() => expect(screen.getByText(i18n.t('page.home.title'))).toBeTruthy());
    });
});
