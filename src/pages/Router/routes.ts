import { appRoutes } from './constants';
import { RouteType } from './types';

/**
 * All the possible routes we will have and their structure
 */
export const routes: RouteType[] = [
    {
        path: appRoutes.home.route,
        componentPath: 'Home',
        exact: true,
        component: 'Home',
    },
    {
        path: appRoutes.exercise1.route,
        componentPath: 'NormalRange',
        exact: true,
        component: 'NormalRange',
    },
    {
        path: appRoutes.exercise2.route,
        componentPath: 'FixedValuesRange',
        exact: true,
        component: 'FixedValuesRange',
    },
    {
        path: appRoutes.error.route,
        componentPath: 'Error',
        component: 'Error',
    },
];
