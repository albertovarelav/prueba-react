import React, { ReactElement, Suspense, useMemo } from 'react';

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import { PageContainer, Spinner } from '@/components';
import { BASE_PATH } from '@/const/env';

import { routes } from './routes';

// Basename must start with '/' and not end with '/', we try to secure it with some regex magic
const basePath = BASE_PATH.replace(/^\/?(.+)\/$/, '/$1').replace(/\/{1,}/g, '/');

/**
 * Component in charge of loading the pages on demand and gathering them inside the router
 * @returns ReactElement
 */
export function AppRouter(): ReactElement {
    const routesCollection = useMemo(
        () =>
            routes.map((route) => {
                const LazyComponent: React.LazyExoticComponent<any> = React.lazy(() =>
                    import(`../${route.componentPath}/${route.component}.tsx`).then((module) => {
                        return {
                            default: module[route.component],
                        };
                    }),
                );

                const pageContainerComponent: ReactElement = (
                    <Suspense fallback={<Spinner absolute />}>
                        <PageContainer>
                            <LazyComponent />
                        </PageContainer>
                    </Suspense>
                );

                const routeComp = <Route key={route.path} path={route.path} element={pageContainerComponent} />;

                return routeComp;
            }),
        [],
    );

    return (
        <Router basename={basePath}>
            <Routes>{routesCollection}</Routes>
        </Router>
    );
}
