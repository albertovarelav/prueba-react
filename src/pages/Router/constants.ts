import { t } from 'i18next';

import { AppRoutes } from './types';

/**
 * The entire app routes should be added here first and use them trought this object
 */
export const appRoutes: AppRoutes = {
    home: {
        route: '/',
        title: t('page.home.title'),
    },
    exercise1: {
        route: '/exercise1',
        title: t('page.home.title'),
    },
    exercise2: {
        route: '/exercise2',
        title: t('page.home.title'),
    },
    error: {
        route: '*',
        title: t('page.error.title'),
    },
};
