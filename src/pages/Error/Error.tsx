import React, { ReactElement } from 'react';

import { useTranslation } from 'react-i18next';

/**
 * When a route is not found, this component will be loaded
 * @returns ReactElement
 */
export const Error = (): ReactElement => {
    const [t] = useTranslation();

    return <h1>{t('page.error.title')}</h1>;
};
