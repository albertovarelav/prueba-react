import React, { ReactElement } from 'react';

import { useTranslation } from 'react-i18next';

/**
 * Component that will be called from the root url of the application
 * @returns ReactElement
 */
export const Home = (): ReactElement => {
    const [t] = useTranslation();

    return (
        <>
            <h1>{t('page.home.title')}</h1>
        </>
    );
};
