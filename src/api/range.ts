import { Range } from '@/models';

import { get } from './base';
import { ranges } from './iris';

export async function getRange(rangeId: string): Promise<Range> {
    return await get(`${ranges}/${rangeId}`, null, true);
}
