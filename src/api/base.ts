import axios, { AxiosInstance, RawAxiosRequestConfig } from 'axios';

export interface ContextOptions {
    auth?: string;
    baseUrl: string;
    customerContext?: string;
}
let serviceInstance: AxiosInstance;
let controller;

export function initialize(options: ContextOptions): boolean {
    serviceInstance = axios.create({
        baseURL: options.baseUrl,
    });

    return true;
}

export async function get<T>(url: string, options?: RawAxiosRequestConfig, cancelPrev?: boolean): Promise<T> {
    try {
        if (!!cancelPrev && cancelPrev) {
            // Check if an AbortController instance has been assigned to the controller variable, then call the abort method when true.
            // To cancel a pending request the abort() method is called.
            if (!!controller) controller.abort();
            // A new instance of the AbortController is created before making the API request
            controller = new AbortController();
            // grab a reference to its associated AbortSignal object using the AbortController.signal property
            options = !options ? { signal: controller.signal } : { ...options, signal: controller.signal };
        }
        const response = await serviceInstance.get(url, options);
        return response.data;
    } catch (error) {
        // If the request has been expressly cancelled we do not show error to the user
        if (axios.isCancel(error)) {
            throw error;
        }
        const err = (error.response && error.response.data) || error;
        delete err.config;
        throw err;
    }
}
