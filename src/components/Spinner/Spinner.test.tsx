import React from 'react';

import { render, screen } from '@testing-library/react';

import { Spinner } from './Spinner';

describe('Spinner tests', () => {
    test('should render a Spinner', async () => {
        render(<Spinner />);
        expect(screen.getByTestId('spinnerContainer')).toBeTruthy();
    });

    test('should render an absolute Spinner', async () => {
        render(<Spinner absolute />);
        expect(screen.getByTestId('spinnerContainerAbsolute')).toBeTruthy();
    });
});
