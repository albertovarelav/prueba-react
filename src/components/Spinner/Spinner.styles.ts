import { keyframes } from '@emotion/react';
import styled from '@emotion/styled';

const spinAnimation = keyframes`
0% { transform: rotate(0deg); }
100% { transform: rotate(360deg); }
`;

export const SpinnerContainer = styled('div')(() => ({
    display: 'flex',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
}));

export const SpinnerContainerAbsolute = styled('div')(() => ({
    position: 'fixed',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    zIndex: 1,
}));

export const SpinnerStyled = styled('div')(() => ({
    border: '0.5rem solid #f3f3f3',
    borderRadius: '50%',
    borderTop: '0.5rem solid #000',
    width: '3rem',
    height: '3rem',
    animationName: spinAnimation,
    animationDuration: '2s',
    animationIterationCount: 'infinite',
}));
