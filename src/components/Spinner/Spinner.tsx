import React, { ReactElement } from 'react';

import { SpinnerProps } from './Spinner.props';
import { SpinnerContainer, SpinnerContainerAbsolute, SpinnerStyled } from './Spinner.styles';

/**
 * Basic spinner used to show that content is being loaded
 * @param props.abolute Optional field to display the spinner in a container with or without absolute position
 * @returns ReactElement
 */
export const Spinner = ({ absolute }: SpinnerProps): ReactElement => {
    return !!absolute ? (
        <SpinnerContainerAbsolute data-testid="spinnerContainerAbsolute">
            <SpinnerStyled />
        </SpinnerContainerAbsolute>
    ) : (
        <SpinnerContainer data-testid="spinnerContainer">
            <SpinnerStyled />
        </SpinnerContainer>
    );
};

Spinner.defaultProps = {
    absolute: false,
};
