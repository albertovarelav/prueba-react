/**
 * Props accepted by the Spinner component
 */
export interface SpinnerProps {
    absolute?: boolean;
}
