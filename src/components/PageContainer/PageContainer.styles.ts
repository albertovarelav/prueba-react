import styled from '@emotion/styled';

export const PageContainerStyled = styled('div')(() => ({
    minHeight: '100vh',
    backgroundColor: '#fff',
}));

export const MainContainer = styled('div')(() => ({
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
}));

export const Content = styled('div')(() => ({
    maxWidth: '72rem',
    width: '100%',
    textAlign: 'left',
    padding: '1rem 2rem',
}));
