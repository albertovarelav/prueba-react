import React, { ReactElement } from 'react';

import { NavBar } from '../NavBar/NavBar';

import { PageContainerProps } from './PageContainer.props';
import { Content, MainContainer, PageContainerStyled } from './PageContainer.styles';

/**
 * Container used to load each of the pages of the application
 * @param props.children ReactNode to be displayed in the body of the page
 * @returns ReactElement
 */
export const PageContainer = ({ children }: PageContainerProps): ReactElement => {
    return (
        <PageContainerStyled data-testid="pageContainer">
            <NavBar />
            <MainContainer>
                <Content>{children}</Content>
            </MainContainer>
        </PageContainerStyled>
    );
};
