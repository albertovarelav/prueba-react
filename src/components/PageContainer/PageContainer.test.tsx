import React from 'react';

import { render, screen } from '@testing-library/react';

import { TestingWrappers } from '@/utils/testing';

import { PageContainer } from './PageContainer';

describe('PageContainer tests', () => {
    test('should render a PageContainer and its children', async () => {
        render(
            <PageContainer>
                <div>Text</div>
            </PageContainer>,
            { wrapper: TestingWrappers },
        );
        expect(screen.getByTestId('pageContainer')).toBeTruthy();
        expect(screen.getByText('Text')).toBeTruthy();
    });
});
