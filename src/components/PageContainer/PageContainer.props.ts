import { ReactNode } from 'react';

/**
 * Props supported by PageContainer component
 */
export interface PageContainerProps {
    children: ReactNode;
}
