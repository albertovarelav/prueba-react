import styled from '@emotion/styled';

export const NavBarContainer = styled('div')(() => ({
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'space-between',
    padding: '1rem 2rem',
}));

export const MenuContainer = styled('ul')(() => ({
    display: 'flex',
    height: '4rem',
    margin: '0 0 0 1rem',
    padding: 0,
    listStyle: 'none',
}));

export const MenuItem = styled('li')(() => ({
    cursor: 'pointer',
    fontSize: '0.9rem',
    lineHeight: '1.1rem',
    letterSpacing: '.02rem',
    listStyle: 'none',
    margin: '0 1rem',
    '& a': {
        alignItems: 'center',
        color: '#000',
        display: 'flex',
        height: '100%',
        textDecoration: 'none',
    },
}));
