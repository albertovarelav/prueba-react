import React, { ReactElement } from 'react';

import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import AppLogo from '@/assets/svgs/logo.svg';
import { appRoutes } from '@/pages/Router';

import { MenuItem, MenuContainer, NavBarContainer } from './NavBar.styles';

/**
 * Top menu of the application
 * @returns ReactElement
 */
export const NavBar = (): ReactElement => {
    const [t] = useTranslation();

    return (
        <NavBarContainer data-testid="navbarContainer">
            <Link to={appRoutes.home.route}>
                <AppLogo width={'7rem'} />
            </Link>
            <MenuContainer>
                <MenuItem>
                    <Link to={appRoutes.exercise1.route}>{t('menu.normalRange')}</Link>
                </MenuItem>
                <MenuItem>
                    <Link to={appRoutes.exercise2.route}>{t('menu.fixedValuesRange')}</Link>
                </MenuItem>
            </MenuContainer>
        </NavBarContainer>
    );
};
