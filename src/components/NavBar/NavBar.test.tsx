import React from 'react';

import { render, screen } from '@testing-library/react';

import { TestingWrappers } from '@/utils/testing';

import { NavBar } from './NavBar';

describe('NavBar tests', () => {
    test('should render a NavBar', async () => {
        render(<NavBar />, { wrapper: TestingWrappers });
        expect(screen.getByTestId('navbarContainer')).toBeTruthy();
    });
});
