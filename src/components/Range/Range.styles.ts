import styled from '@emotion/styled';

export const RangeContainer = styled('div')(() => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    gap: '1.5rem',
}));

export const RangeStyled = styled('div')(() => ({
    height: '1.5rem',
    position: 'relative',
    marginBottom: '2rem',
    marginTop: '2rem',
    width: '100%',
    '&:after': {
        content: "''",
        display: 'inline-block',
        borderRadius: '10rem',
        position: 'absolute',
        width: '100%',
        height: '0.3rem',
        background: '#000',
        top: 'calc(0.75rem - 0.15em)',
    },
}));

export const BulletStyled = styled('div')(() => ({
    borderRadius: '50%',
    height: '1.5rem',
    width: '1.5rem',
    background: '#000',
    position: 'absolute',
    cursor: 'grab',
    transition: '300ms ease-in transform',
    zIndex: 1,
    '&:hover, &:active': {
        transform: 'scale(1.2)',
    },
    '&:active, &:focus': {
        cursor: 'grabbing',
    },
}));

export const InputContainer = styled('div')(() => ({
    display: 'flex',
    width: '6rem',
    alignItems: 'center',
    justifyContent: 'center',
}));

export const InputStyled = styled('input')(() => ({
    border: 'none',
    width: '4rem',
}));
