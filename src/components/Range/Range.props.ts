import { Range } from '@/models';

/**
 * Props supported by RangeProps component
 */
export interface RangeProps {
    children: Range;
}
