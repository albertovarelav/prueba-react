import React from 'react';

import i18n from '@/i18n';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { errorMock, fixedValuesRangeMock, normalRangeMock } from '@/mocks/rangeMock';

import { Range } from './Range';

const dragEventCtorProps = ['clientX', 'clientY'];
export default class DragEventFake extends Event {
    constructor(type, props) {
        super(type, props);
        dragEventCtorProps.forEach((prop) => {
            if (props[prop] != null) {
                this[prop] = props[prop];
            }
        });
    }
}

describe('PageContainer tests', () => {
    beforeAll(() => {
        Object.defineProperty(HTMLElement.prototype, 'offsetWidth', {
            configurable: true,
            value: 1920,
        });
    });
    test('should render a normal Range', async () => {
        render(<Range>{normalRangeMock}</Range>);
        expect(screen.getByTestId('normalRange')).toBeTruthy();
    });

    test('should render a fixed values Range', async () => {
        render(<Range>{fixedValuesRangeMock}</Range>);
        expect(screen.getByTestId('fixedValuesRange')).toBeTruthy();
    });

    test('should render a message if values are incorrect', async () => {
        render(<Range>{errorMock}</Range>);
        expect(screen.getByText(i18n.t('common.error.incorrectFormat'))).toBeTruthy();
    });

    test('should change the minimum value in a normal Range using input', async () => {
        render(<Range>{normalRangeMock}</Range>);

        const normalRange = screen.getByTestId('normalRange');
        const minLabel = screen.getByTestId('minLabel');
        expect(minLabel.querySelector('span')).toBeTruthy();

        userEvent.click(minLabel);
        await waitFor(() => expect(minLabel.querySelector('input')).toBeTruthy());
        expect(minLabel.querySelector('span')).toBeFalsy();

        const minInput = minLabel.querySelector('input');
        await userEvent.type(minInput, '1');
        await waitFor(() => expect(minInput.value).toBe('11'));
        userEvent.click(normalRange);

        await waitFor(() => expect(minLabel.querySelector('span')).toBeTruthy());
    });

    test('should change the maximun value in a normal Range using input', async () => {
        render(<Range>{normalRangeMock}</Range>);

        const normalRange = screen.getByTestId('normalRange');
        const maxLabel = screen.getByTestId('maxLabel');
        expect(maxLabel.querySelector('span')).toBeTruthy();

        userEvent.click(maxLabel);
        await waitFor(() => expect(maxLabel.querySelector('input')).toBeTruthy());
        expect(maxLabel.querySelector('span')).toBeFalsy();

        const maxInput = maxLabel.querySelector('input');
        await userEvent.type(maxInput, '{backspace}');
        await waitFor(() => expect(maxInput.value).toBe('10'));

        userEvent.click(normalRange);

        await waitFor(() => expect(maxLabel.querySelector('span')).toBeTruthy());
    });

    test("shouldn't interpolate values", async () => {
        render(<Range>{normalRangeMock}</Range>);

        const minLabel = screen.getByTestId('minLabel');

        userEvent.click(minLabel);
        await waitFor(() => expect(minLabel.querySelector('input')).toBeTruthy());

        const minInput = minLabel.querySelector('input');
        await userEvent.type(minInput, '1');
        await waitFor(() => expect(minInput.value).toBe('11'));

        const maxLabel = screen.getByTestId('maxLabel');

        userEvent.click(maxLabel);
        await waitFor(() => expect(maxLabel.querySelector('input')).toBeTruthy());

        const maxInput = maxLabel.querySelector('input');
        await userEvent.type(maxInput, '{backspace}');
        await waitFor(() => expect(maxInput.value).toBe('12'));

        await userEvent.type(minInput, '11');
        await waitFor(() => expect(minInput.value).toBe('11'));
    });

    test("shouldn't change values in a fixed values Range using input", async () => {
        render(<Range>{fixedValuesRangeMock}</Range>);

        const minLabel = screen.getByTestId('minLabel');
        expect(minLabel.querySelector('span')).toBeTruthy();
        fireEvent.click(minLabel);

        await waitFor(() => expect(minLabel.querySelector('input')).toBeFalsy());

        const maxLabel = screen.getByTestId('maxLabel');
        expect(maxLabel.querySelector('span')).toBeTruthy();
        fireEvent.click(maxLabel);

        await waitFor(() => expect(maxLabel.querySelector('input')).toBeFalsy());
    });

    test('should change min value on drag on normal Range', async () => {
        render(<Range>{normalRangeMock}</Range>);

        const range = screen.getByTestId('range');
        const minBullet = screen.getByTestId('minBullet');

        /* @ts-expect-error we need to set DragEvent to a custom one on tests to prove it */
        window.DragEvent = DragEventFake;

        fireEvent.drag(minBullet, {
            dataTransfer: {
                setData: jest.fn(),
            },
            clientX: 1920,
            clientY: 0,
        });
        fireEvent.dragOver(range);

        const minLabelText = screen.getByTestId('minLabel').querySelector('span').textContent;

        await waitFor(() => expect(minLabelText).toBe(`${normalRangeMock.max - 1} €`));
    });

    test('should change max value on drag on normal Range', async () => {
        render(<Range>{normalRangeMock}</Range>);

        const range = screen.getByTestId('range');
        const maxBullet = screen.getByTestId('maxBullet');

        /* @ts-expect-error we need to set DragEvent to a custom one on tests to prove it */
        window.DragEvent = DragEventFake;

        fireEvent.drag(maxBullet, {
            dataTransfer: {
                setData: jest.fn(),
            },
            clientX: 920,
            clientY: 0,
        });
        fireEvent.dragOver(range);

        const maxLabelText = screen.getByTestId('maxLabel').querySelector('span').textContent;

        await waitFor(() => expect(maxLabelText).not.toBe(`${normalRangeMock.max} €`));
    });

    test('should change min value on drag on fixed values Range', async () => {
        render(<Range>{fixedValuesRangeMock}</Range>);

        const range = screen.getByTestId('range');
        const minBullet = screen.getByTestId('minBullet');

        /* @ts-expect-error we need to set DragEvent to a custom one on tests to prove it */
        window.DragEvent = DragEventFake;

        fireEvent.drag(minBullet, {
            dataTransfer: {
                setData: jest.fn(),
            },
            clientX: 1920,
            clientY: 0,
        });
        fireEvent.dragOver(range);

        const minLabelText = screen.getByTestId('minLabel').querySelector('span').textContent;

        await waitFor(() => expect(minLabelText).toBe(`${fixedValuesRangeMock.max} €`));
    });

    test('should change max value on drag on fixed values Range', async () => {
        render(<Range>{fixedValuesRangeMock}</Range>);

        const range = screen.getByTestId('range');
        const maxBullet = screen.getByTestId('maxBullet');

        /* @ts-expect-error we need to set DragEvent to a custom one on tests to prove it */
        window.DragEvent = DragEventFake;

        fireEvent.drag(maxBullet, {
            dataTransfer: {
                setData: jest.fn(),
            },
            clientX: 920,
            clientY: 0,
        });
        fireEvent.dragOver(range);

        const maxLabelText = screen.getByTestId('maxLabel').querySelector('span').textContent;

        await waitFor(() => expect(maxLabelText).toBe(`${fixedValuesRangeMock.rangeValues[3]} €`));
    });
});
