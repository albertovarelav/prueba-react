/**
 * Structure used for the maximum and minimum value of a range
 */
export interface RangeValuesState {
    max: number;
    min: number;
}

/**
 * Structure used to determine whether an input should be displayed or not
 */
export interface RangeShowInput {
    max: boolean;
    min: boolean;
}

/**
 * Data structure returned by hook useRangeData
 */
export interface UseRangeDataResponse {
    rangeValues: RangeValuesState;
    showInput: RangeShowInput;
    rangeRef;
    handleClick: (rangeType: 'max' | 'min') => void;
    handleBlur: (rangeType: 'max' | 'min') => void;
    getPercent: (value: number) => number;
    handleDrag: (event) => void;
    handleDragOver: (event) => void;
    handleChange: (event) => void;
}
