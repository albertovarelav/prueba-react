import { useCallback, useEffect, useRef, useState } from 'react';

import { Range } from '@/models';
import { RangeShowInput, RangeValuesState, UseRangeDataResponse } from './useRangeData.props';

/**
 * Contains all the logic used in the Range component
 * @param {Range} range Range that the component receives
 * @returns UseRangeDataResponse
 */
export const useRangeData = (range: Range): UseRangeDataResponse => {
    const [rangeValues, setRangeValues] = useState<RangeValuesState>();

    const [showInput, setShowInput] = useState<RangeShowInput>({ min: false, max: false });

    const rangeRef = useRef(null);

    const validateValue = useCallback(
        (value: number, type: 'max' | 'min'): number => {
            if (value < 0) return;
            if (value < range.min) value = range.min;
            if (value > range.max) value = range.max;

            if (type === 'min' && value >= rangeValues.max - 1) {
                // We add a variation of minus 1 between the maximum and the minimum so that they do not have the same value and it is a search between two numbers.
                value = Math.abs(rangeValues.max) - 1;
            }

            if (type === 'max' && value <= rangeValues.min + 1) {
                // We add a variation of plus 1 between the maximum and the minimum so that they do not have the same value and it is a search between two numbers.
                value = Math.abs(rangeValues.min) + 1;
            }

            return value;
        },
        [range.max, range.min, rangeValues?.max, rangeValues?.min],
    );

    const handleChange = useCallback(
        (event): void => {
            const { name, value } = event.target;
            const validatedValue = validateValue(value, name);

            if (!validatedValue) return;

            setRangeValues({ ...rangeValues, [name]: validatedValue });
        },
        [rangeValues, validateValue],
    );

    const handleDrag = useCallback(
        (event): void => {
            event.preventDefault();

            const newValueByPosition = event.clientX - rangeRef.current.getBoundingClientRect().x;
            const percentage = (newValueByPosition / rangeRef.current.offsetWidth) * range.max;

            let validatedValue = validateValue(percentage, event.target.dataset.slider);

            if (!validatedValue) return;

            if (!range?.rangeValues) {
                validatedValue = Math.round(validatedValue * 100) / 100;
            }

            if (range?.rangeValues) {
                validatedValue = range.rangeValues.reduce(function (prev, curr) {
                    return Math.abs(curr - validatedValue) < Math.abs(prev - validatedValue) ? curr : prev;
                });
            }

            setRangeValues({ ...rangeValues, [event.target.dataset.slider]: validatedValue });
        },
        [range?.max, range?.rangeValues, rangeValues, validateValue],
    );

    const handleDragOver = useCallback((event): void => {
        event.preventDefault();
    }, []);

    const handleClick = useCallback(
        (rangeType: 'max' | 'min'): void => {
            setShowInput({ ...showInput, [rangeType]: true });
        },
        [showInput],
    );
    const handleBlur = useCallback(
        (rangeType: 'max' | 'min'): void => {
            setShowInput({ ...showInput, [rangeType]: false });
        },
        [showInput],
    );

    const getPercent = useCallback(
        (value: number) => {
            const percent = ((value - range.min) / (range.max - range.min)) * 100;

            return percent;
        },
        [range.max, range.min],
    );

    useEffect(() => {
        if (range.min > range.max) return;

        setRangeValues({ min: range.min, max: range.max });
    }, [range]);

    return {
        rangeValues,
        showInput,
        rangeRef,
        handleClick,
        handleBlur,
        getPercent,
        handleDrag,
        handleDragOver,
        handleChange,
    };
};
