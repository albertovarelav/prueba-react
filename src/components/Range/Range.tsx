import React, { ReactElement } from 'react';

import { useTranslation } from 'react-i18next';

import { useRangeData } from './hooks/useRangeData';
import { RangeProps } from './Range.props';
import { BulletStyled, RangeStyled, RangeContainer, InputStyled, InputContainer } from './Range.styles';

/**
 * Component that is in charge of rendering a range slider, which can be of Normal or Fixed Values type
 * @param {Range} props.children Range sent to the component
 * @returns ReactElement
 */
export const Range = ({ children: range }: RangeProps): ReactElement => {
    const [t] = useTranslation();
    const {
        rangeValues,
        showInput,
        rangeRef,
        handleClick,
        handleBlur,
        getPercent,
        handleDrag,
        handleDragOver,
        handleChange,
    } = useRangeData(range);

    return (
        <div>
            {rangeValues && (
                <RangeContainer data-testid={!range?.rangeValues ? 'normalRange' : 'fixedValuesRange'}>
                    <InputContainer>
                        <label
                            onClick={(): void => {
                                !range?.rangeValues && handleClick('min');
                            }}
                            data-testid="minLabel"
                        >
                            {!showInput.min && <span>{rangeValues.min} €</span>}
                            {showInput.min && (
                                <InputStyled
                                    type="number"
                                    name="min"
                                    value={rangeValues.min}
                                    onChange={handleChange}
                                    onBlur={(): void => {
                                        handleBlur('min');
                                    }}
                                />
                            )}
                        </label>
                    </InputContainer>
                    <RangeStyled
                        onDragOver={handleDragOver}
                        onTouchMove={handleDragOver}
                        ref={rangeRef}
                        data-testid="range"
                    >
                        <BulletStyled
                            style={{
                                left: `${getPercent(rangeValues.min)}%`,
                                zIndex: getPercent(rangeValues.min) === getPercent(range.max) ? 2 : 1,
                            }}
                            onDrag={handleDrag}
                            draggable
                            data-slider="min"
                            data-testid="minBullet"
                        />
                        <BulletStyled
                            style={{
                                left: `${getPercent(rangeValues.max)}%`,
                            }}
                            onDrag={handleDrag}
                            draggable
                            data-slider="max"
                            data-testid="maxBullet"
                        />
                        <div className="range-line" />
                    </RangeStyled>
                    <InputContainer>
                        <label
                            onClick={(): void => {
                                !range?.rangeValues && handleClick('max');
                            }}
                            data-testid="maxLabel"
                        >
                            {!showInput.max && <span>{rangeValues.max} €</span>}
                            {showInput.max && (
                                <InputStyled
                                    type="number"
                                    name="max"
                                    value={rangeValues.max}
                                    onChange={handleChange}
                                    onBlur={(): void => {
                                        handleBlur('max');
                                    }}
                                />
                            )}
                        </label>
                    </InputContainer>
                </RangeContainer>
            )}

            {!rangeValues && <>{t('common.error.incorrectFormat')}</>}
        </div>
    );
};
