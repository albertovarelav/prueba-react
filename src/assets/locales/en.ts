export const en = {
    translations: {
        common: {
            error: {
                incorrectFormat: 'An error has occurred with the request',
            },
        },
        page: {
            home: {
                title: 'Mango React test',
            },
            exercise1: {
                title: 'Normal Range',
            },
            exercise2: {
                title: 'Fixed values Range',
            },
            error: {
                title: '404 Page not found',
            },
        },
        menu: {
            normalRange: 'Exercise 1',
            fixedValuesRange: 'Exercise 2',
        },
    },
};
