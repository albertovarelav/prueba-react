export const es = {
    translations: {
        common: {
            error: {
                incorrectFormat: 'Ha ocurrido un error con la petición',
            },
        },
        page: {
            home: {
                title: 'Prueba React Mango',
            },
            error: {
                title: '404 Página no encontrada',
            },
        },
        menu: {
            normalRange: 'Ejercicio 1',
            fixedValuesRange: 'Ejercicio 2',
        },
    },
};
