/**
 * API Url obtained from the .env file
 */
export const API_URL = process.env.API_URL;

/**
 * Base app path obtained from the .env file
 */
export const BASE_PATH = process.env.BASE_PATH;
