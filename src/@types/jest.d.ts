declare namespace jest {
    interface Matchers<R> {
        toHaveClass(...classNames: string[]): R;
    }
}
