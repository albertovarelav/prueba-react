import React from 'react';

import { I18nextProvider } from 'react-i18next';

import i18n from '@/i18n';
import { initialize } from '@/api/base';
import { API_URL } from '@/const/env';
import { AppRouter } from '@/pages/Router';

import './App.css';
import { createRoot } from 'react-dom/client';

initialize({
    baseUrl: API_URL,
});

const container = document.getElementById('app');
const root = createRoot(container);

root.render(
    <I18nextProvider i18n={i18n}>
        <AppRouter />
    </I18nextProvider>,
);
