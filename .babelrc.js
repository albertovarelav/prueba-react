const presets = ['@babel/preset-env', '@babel/preset-react', '@babel/preset-typescript'];
const plugins = [
    'transform-class-properties',
    '@babel/plugin-proposal-object-rest-spread',
    '@babel/plugin-syntax-dynamic-import',
    '@babel/plugin-transform-runtime',
    '@babel/plugin-transform-typescript',
];

module.exports = { presets, plugins };
